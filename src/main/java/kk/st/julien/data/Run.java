package kk.st.julien.data;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Run {
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("H:m:s");
    private String name;
    private LocalTime duration;

    public Run(String name, LocalTime duration) {
        this.name = name;
        this.duration = duration;
    }

    public Run(String name, String duration) {
        this.name = name;
        this.duration = convert(duration);
    }

    private static LocalTime convert(String duration) {
        if("--".equals(duration)){
            return null;
        }
        long count = duration.chars().filter(c -> c == ':').count();
        if(count<2)
            duration="0:"+duration;
        return LocalTime.parse(duration, FORMATTER);
    }

    public String getName() {
        return name;
    }

    public LocalTime getDuration() {
        return duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Run run = (Run) o;
        return Objects.equals(name, run.name) &&
            Objects.equals(duration, run.duration);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, duration);
    }

    @Override
    public String toString() {
        return "Run{" +
            "name='" + name + '\'' +
            ", duration=" + duration +
            '}';
    }

    public String getDurationText() {
        if(duration==null)
            return "";
        return FORMATTER.format(duration);
    }
}
