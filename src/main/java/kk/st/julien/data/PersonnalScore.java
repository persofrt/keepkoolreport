package kk.st.julien.data;

import java.util.LinkedHashSet;
import java.util.Set;

public class PersonnalScore {
    private String name;
    private String age;
    private String gender;
    private Set<Run> runs;

    public PersonnalScore(String name, String age, String gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        runs = new LinkedHashSet<>();
    }

    public void addRun(Run toAdd){
        runs.add(toAdd);
    }

    @Override
    public String toString() {
        return "PersonnalScore{" +
            "name='" + name + '\'' +
            ", age='" + age + '\'' +
            ", gender='" + gender + '\'' +
            ", runs=" + runs +
            '}';
    }

    public String getName() {
        return name;
    }

    public String getAge() {
        return age;
    }

    public String getGender() {
        return gender;
    }

    public Set<Run> getRuns() {
        return runs;
    }
}
