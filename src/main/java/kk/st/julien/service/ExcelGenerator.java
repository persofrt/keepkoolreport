package kk.st.julien.service;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import kk.st.julien.data.PersonnalScore;
import kk.st.julien.data.Run;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ExcelGenerator {
    public void generateReport(String destination, List<PersonnalScore> entry) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(destination))) {
            writer.write(generateHeader(entry));
            writer.newLine();
            entry.forEach(line -> generateLine(writer, line));
        }
    }

    private void generateLine(BufferedWriter writer, PersonnalScore line) {
        try {
            String collect = line.getRuns().stream().map(Run::getDurationText).collect(Collectors.joining(";"));
            writer.write(String.format("%s;%s;%s;%s",line.getAge(), line.getGender(), line.getName(), collect));
            writer.newLine();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String generateHeader(List<PersonnalScore> entry) {
        String sb = "age;sexe;nom;" +
            entry.get(0).getRuns().stream().map(Run::getName).collect(Collectors.joining(";"));
        return sb;
    }
}
