package kk.st.julien;

import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import kk.st.julien.data.PersonnalScore;
import kk.st.julien.data.Run;
import kk.st.julien.service.ExcelGenerator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    private final static String expressoUrlBase = "https://expresso.com";
    private final static String rootUrl = expressoUrlBase+"/Challenge/Riders/TourdeSaintJulien";
    private final static String destination = "/home/fred/tmp/report.csv";

    private List<PersonnalScore> report = new ArrayList<>();

    private ExcelGenerator generator = new ExcelGenerator();

    public static void main(String[] args) throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        Main m = new Main();
        m.process();
        System.out.println(m.report);
    }

    private void process() throws IOException, CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {
        int counter =0;
        while(counter!=-1){
            counter = parsePage(counter);
        }
        generator.generateReport(destination, report);
    }

    private PersonnalScore parsePlayer(PersonnalScore ps, String playerUrl){
        try {
            System.out.println("on traite "+ps.getName());
            Document doc = Jsoup.connect(playerUrl).get();
            Elements runDiv = doc.select("#TabStages div.list-item");
            runDiv.forEach(row -> parseRow(row.selectFirst("div.row"), ps));
            return ps;
        }catch (Exception e){
            throw new RuntimeException(e);
        }
    }

    private void parseRow(Element row, PersonnalScore ps) {
        Element name = row.selectFirst("div:nth-child(2)");
        Element time = row.selectFirst("div:nth-child(4)");
        Run run = new Run(name.ownText(), time.text());
        ps.addRun(run);
    }

    private int parsePage(int page) throws IOException {
        String conUrl = rootUrl;
        if(page!=0){
            conUrl+="?page="+page;
        }
        System.out.println("on crawle "+conUrl);
        Document doc = Jsoup.connect(conUrl).get();
        Elements lines = doc.select("div.challenge-standings-item");
        if(lines.size()!=0){
            report.addAll(lines.stream().map(this::parseMainLine).collect(Collectors.toList()));
            return(page==0?2:page+1);
        }else{
            return -1;
        }
    }

    private PersonnalScore parseMainLine(Element line){
        Element sub = line.selectFirst("div.row > div:nth-child(2) > div.row > div:nth-child(2)");
        Element element = sub.selectFirst("div.rider a");
        String href = element.attr("href");
        String name = element.text();
        Element info = sub.selectFirst("div.info");
        String age = info.selectFirst("span.age").text();
        String gender = info.selectFirst("span.gender > i").className().contains("fa-male")?"Homme":"Femme";
        PersonnalScore ps = new PersonnalScore(name, age, gender);
        return parsePlayer(ps, expressoUrlBase+href);
    }

}
